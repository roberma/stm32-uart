/*
 *	Header file for final blink program
 */
#ifndef __BLINK_H
#define __BLINK_H

#include <stm32l476xx.h> //main library for STM32 microcontroller

#define INTERVAL	1000	// Interval in ms to toggle LD2
#define BUFSIZE		10	// UART buffer size
#define BAUDRATE	9600	// default UART baud rate
#define MAXINT	60000	// max interval value is restricted to 60 sec
void initIO();	// Initialise GPIO
void initTimer(uint16_t ms);	// Initialise Timer
void setInterval(uint16_t ms);	// Set new timer interval value
void TIM6_IRQHandler();	// Timer handler toggles LD2 when timer expire and update interrupt is catched


void initUART();	// Init UART
void sendChar(int ch);	// Send character to UART
int getChar(void);	// Receive character from UART
void initIO();	// Initialise GPIO port A pin 5 to manage LD2 
void sendMessage(const char *message);	// send char string to UART

char	buffer[BUFSIZE];	// char buffer to save readed characters from UART
int		readPos,	// buffer position where next received character from UART must be saved
		cr;	// flag set to 1, if cr symbol found

/* 	current state of stream
 *	READING - waiting more input characters, end of stream not found
 *	TOOLONG - input stream exceeds buffer size
 *	END - end of stream character found
 */
enum state {READING, TOOLONG, END}curState;	

#endif