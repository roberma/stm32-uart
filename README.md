# UART + IRQ + TIM
## Task
Use UART communication to control time blinking frequency of LED LD2 on NUCLEO-L476RG development board. Use timer and timer interrupts. Use UART interrupts.

## Task Solution
These step were made to use UART input value to change blink interval of LD2 :
- Configure GPIO port A pin 5 to toggle LD2 (general purpose output, push pull, low speed);
- Configure timer TIM6 to send update interrupt after interval set (Initial interval - 1000 ms);
- Create UART read buffer;
- Configure GPIO port A pin 2 as USART2 TX pin (Alternate function output, push pull, high speed);
- Configure GPIO port A pin 3 as USART2 RX pin (Alternate function output, open drain, high speed, pull up activated);
- Set baud rate divider to baud rate 9600 for bus clock frequency of 4 MHz;
- Enable RXE interrupts;
- Enable USART2 TX and RX;
- Start USART2;
- Implement functions to send character to UART and to receive character from UART;
- Implement USART2 IRQHandler. Read data stream from UART until end of stream symbol found. Check if buffer size is not exceeded;
- Convert input stream to numeric value. Check, if new interval value is a positive number and not bigger than 60000;
- Set new value to timer and generate an update event to restart the timer.
- Implement a function to send a character string message to UART.

**Source code can be found [here](src/)**

**Include file can be found [here](include/blink.h)**
