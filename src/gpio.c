
/*
 *	GPIO routines for final blink program
 */

#include "blink.h"

/*
 *	Initialise GPIO port A pin 5 to manage LD2
 */
void initIO()
{
	// Enable IO port A peripheral clock
 	RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;

	// Configure IO port A pin 5 mode to general purpose output
 	GPIOA->MODER &= (~GPIO_MODER_MODE5);
	GPIOA->MODER |= GPIO_MODER_MODE5_0;

	// Configure IO port A pin 5 output type to push-pull (can be skipped, because is default set after reset) 
 	GPIOA->OTYPER &= ~GPIO_OTYPER_OT5;

	// Configure IO port A pin 5 output speed to slow (can be skipped, because is default set after reset)
 	GPIOA->OSPEEDR &= ~GPIO_OSPEEDR_OSPEED5;

	// Configure IO port A pin 5 not to use pull-up and pull-down (can be skipped, because is default set after reset)
 	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPD5;
}



