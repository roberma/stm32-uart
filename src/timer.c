
/*
 *	Timer routines for final blink program
 */

#include "blink.h"

/*
 *	Initialise basic Timer TIM6
 *	ms - time in ms, when the timer expires and update flag is set
 */
void initTimer(uint16_t ms)
{
	// Enable TIM6 timer clock
	RCC->APB1ENR1 |= RCC_APB1ENR1_TIM6EN;

	// Set timer prescaler to scale timer clock from 4 MHz to 1 kHz. In this way, the timer counter is incremented every milisecond
	TIM6->PSC = (SystemCoreClock / 1000) - 1;
	
	// Set timer auto reload register to the count in miliseconds, after that the timer is restarted and event update flag is set
	TIM6->ARR = ms; 

	// Enable Timer Update interrupt
	TIM6->DIER |= TIM_DIER_UIE;
	
	// Enable general IRQ for TIM16
	NVIC_EnableIRQ(TIM6_IRQn);

	// Set the timer enable flag and so start the timer
	TIM6->CR1 |= TIM_CR1_CEN;

	// Clear status register
	TIM6->SR  =  0; 
}

/*
 *	Set new timer value to count to
 *	ms - new time value in ms
 */
void setInterval(uint16_t ms)
{
	TIM6->ARR = ms; 
	TIM6->EGR  = TIM_EGR_UG; 
}

/*
 *	TIM6 Interrupt Handler
 *	Catches Update interrupt and then toggles LD2
 */
void TIM6_IRQHandler()
{
	// Check if it is TIM6 Update Interrupt
	if (TIM6->SR & TIM_SR_UIF)
	{
		TIM6->SR &= ~TIM_SR_UIF;	// Clear update event flag
		GPIOA->ODR ^= GPIO_ODR_OD5;	// Toggle LD2
	}
}
