/*
 *	UART routines for final blink program
 */

#include "blink.h"
#include <string.h>

/*
 *	Configure PA2 pin as USART2 TX, PA3 pin as USART RX and setup USART2 for baudrate brate
 *	baudrate - UART init baud rate value
 */
void initUART(uint32_t baudrate)
{
	readPos = 0; // buffer position
	cr = 0;	// cariage return symbol not detected
	curState = READING;	// waiting incoming stream from UART

	// Enable IO Port A clock
	RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;

	// Configure GPIOA pin 2 as USART2 TX in alternate function mode, high speed, push pull output (reset state)
	GPIOA->MODER &= (~GPIO_MODER_MODE2);
	GPIOA->MODER |= GPIO_MODER_MODE2_1;
	GPIOA->OSPEEDR &= (~GPIO_OSPEEDER_OSPEEDR2);
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR2_1;
	GPIOA->AFR[0] |= GPIO_AFRL_AFSEL2;
	GPIOA->AFR[0] &= (~GPIO_AFRL_AFSEL2_3);

	// Configure GPIO pin 3 as USART2 RX in alternate function mode, high speed, open drain output with pull up
	GPIOA->MODER &= (~GPIO_MODER_MODE3);
	GPIOA->MODER |= GPIO_MODER_MODE3_1;
	GPIOA->OTYPER |= GPIO_OTYPER_OT3;
	GPIOA->OSPEEDR &= (~GPIO_OSPEEDER_OSPEEDR3);
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR3_1;
	GPIOA->PUPDR &= (~GPIO_PUPDR_PUPD3);
	GPIOA->PUPDR |= GPIO_PUPDR_PUPD3_0;
	GPIOA->AFR[0] |= GPIO_AFRL_AFSEL3;
	GPIOA->AFR[0] &= (~GPIO_AFRL_AFSEL3_3);	

	// Enable USART2 clock
	RCC->APB1ENR1 |= RCC_APB1ENR1_USART2EN;

	// Set Baud rate to brate by Clock frequency 4 MHz
	USART2->BRR = SystemCoreClock / baudrate; 

	// After reset UART by default is set to 1 start bit, 8 Data bits, 1 stop bit and no parity bit. So we use this configuration.

	// ENable USART2 Interrupts
	USART2->CR1 |= (USART_CR1_RXNEIE); 

	// Enable USART2 Transmiter and Receiver
	USART2->CR1 |= (USART_CR1_TE | USART_CR1_RE);
	// Enable USART2
	USART2->CR1 |= USART_CR1_UE;
	// Enable USART2 interrupts in NVIC
	NVIC_EnableIRQ(USART2_IRQn);
	// Reset status register
	USART2->ISR = 0;
}
/*
 *	USART2 IRQ Handler
 *	Reads next character from buffer and checks if it is not cariage return, linefeed or both
 */
void USART2_IRQHandler()
{

	// There is data to read
	if (USART2->ISR & USART_ISR_RXNE)
	{
		if (readPos < BUFSIZE)
		{
			buffer[readPos] = getChar();
			if(buffer[readPos] == '\n')
			{
				// if last symbol was cariage return, then it is cr/lf sequence and the input is already read
				if(!cr)
				{
					curState = END; // last symbol wasn't cariage return so it is single linefeed symbol
				}
				cr = 0;
			}
			else
			{
				if(buffer[readPos] == '\r')
				{
					cr = 1;	//cariage return symbol found
					curState = END;
				}
				else
				{
					cr = 0;
				}
			}
			readPos++;
		}
		else
		{
			char dummy = getChar(); // discard read character
			curState = TOOLONG;	// input line exceeds buffer size
		}
	}
}

/*
 * send char string to UART
 * message - char string to send
 */
void sendMessage(const char *message)
{
	int len = strlen(message);
	int i;

	// copy message to the send buffer
	for(i = 0; i < len; i++)
	{
		sendChar(message[i]);
	}
}

/*
 *	Send character to UART
 *	ch - character to send
 */
void sendChar(int ch)
{
	// check for errors, in case of error turn LD2 on
	if(USART2->ISR & 0xF)
	{
		GPIOA->BSRR |= GPIO_BSRR_BS5;
	}
	while(!(USART2->ISR & USART_ISR_TXE))	// wait until UART send register will be empty
	;
	USART2->TDR = ch;
}

/*
 *	Read character from UART
 *  Return - readed character
 */
int getChar(void)
{
	// check for errors, in case of error turn LD2 on
	if(USART2->ISR & 0xF)
	{
		GPIOA->BSRR |= GPIO_BSRR_BS5;
	}
	return USART2->RDR;
}
