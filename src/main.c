
/*
 *	Main function and program logic routines for final blink program
 */
#include "blink.h"
#include <stdlib.h>

int main (void) {

	int newTime = 0;	// new interval value send via UART

	// Init GPIO pin for LD2
	initIO();

	// Init timer
	initTimer(INTERVAL);

	// Init USART2 and GPIO pins used for it
	initUART(BAUDRATE);

	sendMessage("Insert new blink interval in ms:\n");
	while(1)
	{
		if(curState)
		{
			switch (curState)
			{
			case END:
				newTime = atoi(buffer);	// convert input character string to time value
				if(newTime > 0)
				{
					if(newTime < MAXINT)
					{
						setInterval(newTime);
						sendMessage("\nNew blink interval set.\n");
					}
					else
					{
						sendMessage("\nBlink interval can't be bigger than 60000 ms.\n");
					}
				}
				else
				{
					sendMessage("\nMust be a positive number.\n");
				}
				break;
			case TOOLONG:
				sendMessage("\nInput too long.\n");
				break;
			}
			curState = READING;
			readPos = 0;
		}
	}
}